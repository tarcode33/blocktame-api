import { Wallet, User } from '../models'
import {
  createWallet as createStellarWallet,
  fundTestnetAccount
} from '../interfaces/stellar'
import {
  createWallet as createEtherWallet
} from '../interfaces/ethereum'

export const getWallets = async (req, res) => {
  const found_wallets = await Wallet.find({email: req.payload.email })

  return res.send(found_wallets)
}

export const generateWallet = async (req, res) => {  
  console.log("THIS IS HAPPENGING", req.body.type);
  
  const user = await User.findOne({ email: req.payload.email })
  const user_id = user._id;

  let wallet

  try {
    if (req.body.type === 'stellar') {
      wallet = createStellarWallet();
      if (process.env.IS_TESTNET) {
       setTimeout(async () => {
        await fundTestnetAccount(wallet.public_key)
       }, 1500);
      }
    } else if (req.body.type === 'ethereum') {
      wallet = createEtherWallet();
    } else {
      return res.status(400).send({
        msg: "Please provide a valid wallet type (stellar, ethereum)"
      })
    }

    const wallet_to_save = new Wallet({
      user_id,
      public_key: wallet.public_key,
      type: req.body.type,
      email: user.email,
      created: new Date(),
      default: false
    })

    wallet_to_save.save()
    
    console.log("WALLET GOT HERE", wallet);
    
    return res.send({
      wallet,
      msg: "Wallet created"
    })
  } catch(err) {
    return res.send(err)
  }
}

export const getRecipient = async (req, res) => {

  const email_or_addr = req.params.recipient;

  let recipient

  let crypto
  if (!email_or_addr.includes('@') && email_or_addr.length === 56) {
    crypto = true
  } else {
    recipient = await Wallet.findOne({email: email_or_addr})
  }

  if (!recipient && !crypto) {
    return res.status(404).send({msg: 'Recipient not found'})
  } else if (crypto && !recipient) {
    return res.send({
      public_key: email_or_addr
    })
  } else {
    return res.send({
      public_key: recipient.public_key,
      email: recipient.email
    })
  }
}
