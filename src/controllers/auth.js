import passport from 'passport'
import fetch from 'node-fetch'
import crypto from 'crypto'

import { User, Token } from '../models'

import { config } from 'dotenv'

config()

const sendgrid_key = process.env.SENDGRID_API_KEY
const sendgrid_url = process.env.SENDGRID_URL

export const register = async (req, res) => {
  const user = {
    email: req.body.email,
    password: req.body.password,
    first_name: req.body.first_name || '',
    last_name: req.body.last_name || '',
    id_number: req.body.id_number || '',
    merchant: req.body.merchant
  }

  if(!user.email) {
    return res.status(400).send({
      status: 'error',
      msg: 'Email is required'
    });
  }

  if(!user.password) {
    return res.status(400).send({
      status: 'error',
      msg: 'Password is required'
    });
  }

  const found_user = await User.findOne({ email: user.email })

  if (found_user) {
    return res.status(404).send({
      status: 'error',
      msg: "User already exists"
    })
  }

  // Create new user
  const finalUser = new User(user);

  // Set new user's password
  finalUser.setPassword(user.password);

  // Save user
  const savedUser = await finalUser.save()

   // Create a verification token for this user
   const token = new Token({ _userId: savedUser._id, token: crypto.randomBytes(16).toString('hex') });
 
   // Save the verification token

   try {
      token.save();
      const data = {
        "personalizations": [
          {
            "to": [
              {
                "email": user.email
              }
            ]
          }
        ],
        "from": {
          "email": "info@blocktame.io"
        },
        "subject": 'Verify your account',
        "content": [
          {
            "type": "text/plain",
            "value": 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttps:\/\/wallet.blocktame.io\/auth/confirmation\/' + token.token + '.\n'
          }
        ]
      }
    
      const response = await fetch(sendgrid_url + 'mail/send', {
          method: "POST",
          mode: "cors",
          headers: {
              "Content-Type": "application/json",
              "Authorization": 'Bearer ' + sendgrid_key
          },
          body: JSON.stringify(data),
      })
      
      console.log("SENDGRID RESPONSE", response);
        
      if (!response.ok) {
          return res.send({msg: "Error sending mail"})
      }
    
      return res.send({msg: "Success: Check your email for further instructions"})
   } catch (err) {
     if (err) { return res.status(500).send({ msg: err.msg }); }
   }
}

export const login = async (req, res, next) => {
  const user = {
    email: req.body.email,
    password: req.body.password
  }

  if(!user.email) {
    return res.status(400).send({
      status: 'error',
      msg: 'Email is required'
    });
  }

  if(!user.password) {
    return res.status(400).send({
      status: 'error',
      msg: 'Password is required'
    });
  }

  return passport.authenticate('local', { session: false }, async (err, passportUser, info) => {
    if(err) {
      return next(err);
    }
    

    if(passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      // Make sure the user has been verified
      if (!user.isVerified) {
        return res.status(401)
               .send({ type: 'not-verified', msg: 'Your account has not been verified.' }); 
      }

      return res.json({ user: user.toAuthJSON() });
    }

    return res.status(400).send({
      status: 'error',
      msg: 'Login credentials are incorrect.'
    });
  })(req, res, next);
}

export const confirmationPost = async (req, res) => {
  if(!req.body.token) {
    return res.status(400).send({
      status: 'error',
      msg: 'Token is required'
    });
  }

  // Find a matching token
  const token = await Token.findOne({ token: req.body.token });

  if (!token) return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });

  // If we found a token, find a matching user
  const user = await User.findOne({ _id: token._userId })

  if (!user) return res.status(400).send({ msg: 'We were unable to find a user for this token.' });
  if (user.isVerified) return res.status(400).send({ type: 'already-verified', msg: 'This user has already been verified.' });

  try {
    user.isVerified = true;
    await user.save()
    res.status(200).send({ msg: "The account has been verified. Please log in."});
  } catch (err) {
    if (err) { return res.status(500).send({ msg: err.msg }); }
  }
};

export const resendTokenPost = async (req, res) => {
  if(!req.body.email) {
    return res.status(400).send({
      status: 'error',
      msg: 'Email is required'
    });
  }

  const user = await User.findOne({ email: req.body.email });

  if (!user) return res.status(400).send({ msg: 'We were unable to find a user with that email.' });
  if (user.isVerified) return res.status(400).send({ msg: 'This account has already been verified. Please log in.' });

  // Create a verification token, save it, and send email
  var token = new Token({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });

  try {
    token.save();
    const data = {
      "personalizations": [
        {
          "to": [
            {
              "email": user.email
            }
          ]
        }
      ],
      "from": {
        "email": "info@blocktame.io"
      },
      "subject": 'Verify your account',
      "content": [
        {
          "type": "text/plain",
          "value": 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttps:\/\/wallet.blocktame.io\/auth/confirmation\/' + token.token + '.\n'
        }
      ]
    }
  
    const response = await fetch(sendgrid_url + 'mail/send', {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + sendgrid_key
        },
        body: JSON.stringify(data),
    })
    
    console.log("SENDGRID RESPONSE", response);
      
    if (!response.ok) {
        return res.send({msg: "Error sending mail"})
    }
  
    return res.send({msg: "Success: Check your email for further instructions"})
 } catch (err) {
   if (err) { return res.status(500).send({ msg: err.msg }); }
 }
};

export const resetPassword = async (req, res) => {
  const user = {
    password: req.body.password
  }

  if(!user.password) {
    return res.status(400).send({
      status: 'error',
      msg: 'Password is required'
    });
  }

  const found_user = await User.findOne({ email: req.payload.email })

  // Set new user's password
  found_user.setPassword(user.password);

  // Save user
  await found_user.save()

  return res.json({ msg: "Password changed!" })
}

export const forgotPassword = async (req, res) => {
  const user = await User.findOne({
    email: req.body.email
  });

  if (!user) {
    res.status(404).send({err: 'User not found.'})
  }

  // create the random token
  const buffer = await crypto.randomBytes(20);
  
  const token = buffer.toString('hex');

  await User.findOneAndUpdate({ _id: user._id }, { reset_password_token: token, reset_password_expires: Date.now() + 86400000 }, { upsert: true, new: true });

  const data = {
    "personalizations": [
      {
        "to": [
          {
            "email": user.email
          }
        ]
      }
    ],
    "from": {
      "email": "info@blocktame.io"
    },
    "subject": 'Password help has arrived!',
    "content": [
      {
        "type": "text/plain",
        "value": 'Click here to reset your password: https://wallet.blocktame.io/password/reset/confirm?token=' + token
      }
    ]
  }

  const response = await fetch(sendgrid_url + 'mail/send', {
      method: "POST",
      mode: "cors",
      headers: {
          "Content-Type": "application/json",
          "Authorization": 'Bearer ' + sendgrid_key
      },
      body: JSON.stringify(data),
  })
  
  console.log("SENDGRID RESPONSE", response);
    
  if (!response.ok) {
      return res.send({msg: "Error sending mail"})
  }

  return res.send({msg: "Success: Check your email for further instructions"})
};

export const resetPasswordConfirm = async (req, res) => {
  const user = await User.findOne({
    reset_password_token: req.body.token,
    reset_password_expires: {
      $gt: Date.now()
    }
  })
  
  if (user) {
    if (req.body.password) {
      await user.setPassword(req.body.password)
      user.reset_password_token = undefined;
      user.reset_password_expires = undefined;
      await user.save();

      const data = {
        "personalizations": [
          {
            "to": [
              {
                "email": user.email
              }
            ]
          }
        ],
        "from": {
          "email": "info@blocktame.io"
        },
        "subject": 'Your password has been reset!',
        "content": [
          {
            "type": "text/plain",
            "value": 'Your password has been reset!'
          }
        ]
      }
    
      const response = await fetch(sendgrid_url + 'mail/send', {
          method: "POST",
          mode: "cors",
          headers: {
              "Content-Type": "application/json",
              "Authorization": 'Bearer ' + sendgrid_key
          },
          body: JSON.stringify(data),
      })
      
        console.log("SENDGRID RESPONSE", response);
          
        if (!response.ok) {
            return res.send({msg: "Error sending mail"})
        }
      
        return res.send({msg: "Success: Your password has been reset"})

      } else {
      return res.status(400).send({
        msg: 'Password reset token is invalid or has expired.'
      });
    }
  } else {
    return res.status(400).send({
      msg: 'Password reset token is invalid or has expired.'
    });
  }
};
