import fetch from 'node-fetch'

const sendgrid_key = process.env.SENDGRID_API_KEY
const sendgrid_url = process.env.SENDGRID_URL


export const sendMail = (req, res) => {

  const { 
      email,
      mobile,
      subject,
      message
  } = req.body 

  const data = {
      "personalizations": [
        {
          "to": [
            {
              "email": "info@blocktame.io"
            }
          ]
        }
      ],
      "from": {
        "email": email
      },
      "subject": subject,
      "content": [
        {
          "type": "text/plain",
          "value": "Mobile: " + mobile + '; ' + message
        }
      ]
    }

  fetch(sendgrid_url + 'mail/send', {
      method: "POST",
      mode: "cors",
      headers: {
          "Content-Type": "application/json",
          "Authorization": 'Bearer ' + sendgrid_key
      },
      body: JSON.stringify(data),
  })
  .then(response => {
      if (!response.ok) {
          return res.send("Error sending mail")
      }
      return res.send("Success")
      
  })
  .catch(err => {
      return res.send("Error sending mail")
  })

}
