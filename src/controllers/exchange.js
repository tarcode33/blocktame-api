import { Transaction, User } from '../models'
import { getExchangeAmount, createOrder as makeOrder, getOrderStatus } from '../interfaces/coinswitch'

export const getExchangeAmt = async (req, res) => {
    const exchange_amount = await getExchangeAmount()
    res.send(exchange_amount)
}

export const createOrder = async (req, res) => {
  const tx = await makeOrder(req.body.address, req.body.amount)
  return res.send(transaction)
}

export const orderStatus = async (req, res) => {
  const status = await getOrderStatus(req.params.orderId)
  return res.send(status)
}