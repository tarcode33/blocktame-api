import { User } from '../models'

export const updateProfile = async (req, res, next) => {
  const user = await User.updateOne({ email: req.payload.email }, {
    $set: {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      id_number: req.body.id_number
    }
  })
  return res.send(user)
}

export const getProfile = async (req, res, next) => {
  const user = await User.findOne({ email: req.payload.email })

  const user_to_send = {
    email: user.email,
    first_name: user.first_name,
    last_name: user.last_name,
    id_number: user.id_number
  }

  return res.send(user_to_send)
}