import {
  login,
  register,
  confirmationPost,
  resendTokenPost,
  resetPassword,
  forgotPassword,
  resetPasswordConfirm
} from './auth'

import {
  getProfile,
  updateProfile
} from './user'

import {
  generateWallet,
  getWallets,
  getRecipient
} from './wallet'

import {
  getExchangeAmt,
  getMinimumAmount,
  createOrder,
  orderStatus,
} from './exchange'

import {
  sendMail
} from './mail'

import {
  getShops,
  getCategories,
  getProducts,
  addShop,
  addCategory,
  addProduct
} from './market'

export {
  login,
  register,
  confirmationPost,
  resendTokenPost,
  resetPassword,
  forgotPassword,
  resetPasswordConfirm,

  getProfile,
  updateProfile,

  generateWallet,
  getWallets,
  getRecipient,

  getExchangeAmt,
  getMinimumAmount,

  createOrder,
  orderStatus,

  sendMail,

  getShops,
  getCategories,
  getProducts,
  addShop,
  addCategory,
  addProduct

}
