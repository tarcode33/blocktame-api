import { 
    Shop,
    Category,
    Product,
    User
} from '../models/'

export const getShops = async (req, res) => {
    const shops = await Shop.find()

    return res.send({ shops })
}

export const getCategories  = async (req, res) => {
    const categories = await Category.find({ shopId: req.params.shopId })

    return res.send({ categories })
}

export const getProducts = async (req, res) => {
    const products = await Product.find({ categoryId: req.params.categoryId })
    return res.send({ products })
}

export const addShop = async (req, res) => {
    if (isMerchant(req.body.merchantId)) {
        const shop_to_save = new Shop({
            merchantId: req.body.merchantId,
            name: req.body.name,
            description: req.body.description,
            img_url: '',
            created: new Date(),
          })
      
          await shop_to_save.save()
    
        return res.send({ msg: "Shop has been added"})
    } else {
        return res.status(401).send({ status: "error", msg: "Only merchants are authorized to add shops"})
    }
}

export const addCategory = async (req, res) => {

    if (isMerchant(req.body.merchantId)) {
        const category_to_save = new Category({
            shopId: req.params.shopId,
            name: req.body.name,
            description: req.body.description,
            created: new Date(),
          })
      
          await category_to_save.save()
    
        return res.send({ msg: "Category has been added"})
    } else {
        return res.status(401).send({ status: "error", msg: "Only merchants are authorized to add categories"})
    }
}

export const addProduct = async (req, res) => {
    if (isMerchant(req.body.merchantId)) {
        const product_to_save = new Product({
            categoryId: req.body.categoryId,
            name: req.body.name,
            description: req.body.description,
            price_in_xlm: req.body.price_in_xlm,
            qty: req.body.qty,
            created: new Date(),
          })
      
          await product_to_save.save()
    
        return res.send({ msg: "Product has been added"})
    } else {
        return res.status(401).send({ status: "error", msg: "Only merchants are authorized to add products"})
    }
}

async function isMerchant(_id) {
    const user = await User.findOne({ _id })

    if (user && user.merchant) {
        return true;
    } else {
        return false;
    }
}