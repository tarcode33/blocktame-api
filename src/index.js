import express from 'express'
import http from 'http'
import socketio from 'socket.io'

import routes from './routes'
import headers from './middleware/headers'

import { syncTransactions } from './controllers'

const app = express()

const server = http.createServer(app)
const io = socketio(server)

console.log("Express server running");

const PORT = 3000

app.get('/', (req, res) => {
  return res.send('Hello World!')
})

app.use(headers)
app.use('/api', routes);

// Websocket logic for live data
io.on("connection", function(socket) {
  var handshakeData = socket.request;

  const addr = handshakeData._query['address'];

  if (addr) {
    console.log("SUPPLIED ADDRESS", addr);
    syncTransactions(addr, true)
  }
  
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('update', function(){
    socket.broadcast.emit('update');
  });
  
});

server.listen(PORT, () => console.log(`Listening on PORT ${PORT}`));
