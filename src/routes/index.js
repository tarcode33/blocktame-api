import express from 'express'
import mongoose from 'mongoose'
import auth from './auth'
import { config } from 'dotenv'

import '../models'
import '../config/passport'


import { 
    register,
    login,
    confirmationPost,
    resendTokenPost,
    resetPassword,
    forgotPassword,
    resetPasswordConfirm,
    updateProfile,
    
    getProfile,
    sendMail,
    getWallets,
    generateWallet,
} from '../controllers'

config()

const app = express()

const uri = process.env.MONGO_URL;

mongoose.connect(uri, { useNewUrlParser: true  });

// public routes
app.post('/auth/register', auth.optional, register);
app.post('/auth/login', auth.optional, login);
app.post('/auth/confirmation', auth.optional, confirmationPost);
app.post('/auth/confirmation/resend', auth.optional, resendTokenPost);

app.post('/auth/password/forgot', auth.optional, forgotPassword);
app.post('/auth/password/reset/confirm', auth.optional, resetPasswordConfirm);
app.post('/auth/password/reset', auth.required, resetPassword);
app.post('/mail', auth.optional, sendMail)

// //  Protected user routes
app.get('/profile/', auth.required, getProfile)
app.post('/profile/', auth.required, updateProfile)

app.get('/wallets/', auth.required, getWallets)

app.post('/wallets/generate', auth.required, generateWallet)

module.exports = app;
