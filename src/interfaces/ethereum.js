import bip39 from 'bip39'
import hdkey from 'hdkey'
import {
    privateToPublic,
    publicToAddress,
    toChecksumAddress
} from 'ethereumjs-util'

// Create function to return mnemonic and 1st derived address + private key
export const createWallet = () => {

    const mnemonic = bip39.generateMnemonic(); //generates string
    const seed = bip39.mnemonicToSeed(mnemonic); //creates seed buffer
    const rootKey = hdkey.fromMasterSeed(seed);
    // const masterPrivateKey = rootKey.privateKey.toString('hex');

    const addrNode = rootKey.derive("m/44'/60'/0'/0/0"); //line 1
    const privKey = addrNode._privateKey.toString('hex');;
    const pubKey = privateToPublic(addrNode._privateKey);
    const addr = publicToAddress(pubKey).toString('hex');
    const public_key = toChecksumAddress(addr);
    return {
        mnemonic,
        public_key,
        privKey
    };
}

// Create function to return 1st derived address and private key from mnemonic
export const restoreWallet = (mnemonic) => {
    const seed = bip39.mnemonicToSeed(mnemonic); //creates seed buffer
    const rootKey = hdkey.fromMasterSeed(seed);
    // const masterPrivateKey = rootKey.privateKey.toString('hex');

    const addrNode = rootKey.derive("m/44'/60'/0'/0/0"); //line 1
    const privKey = addrNode._privateKey.toString('hex');;
    const pubKey = privateToPublic(addrNode._privateKey);
    const addr = publicToAddress(pubKey).toString('hex');
    const address = toChecksumAddress(addr);
    return {
        mnemonic,
        address,
        privKey
    };
}


