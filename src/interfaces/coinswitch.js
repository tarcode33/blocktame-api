import fetch from 'node-fetch'

const coinswitch_url = process.env.COINSWITCH_URL
const coinswitch_key = process.env.COINSWITCH_KEY
const ip_address = process.env.IP_ADDRESS

export const getExchangeAmount = async () => {
  const response = await fetch(coinswitch_url + 'v2/rate', {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': coinswitch_key,
        'x-user-ip': ip_address
      },
      method: 'POST',
      body: JSON.stringify({
        depositCoin: 'btc',
        destinationCoin: 'xlm'
      })
  })
  const json = await response.json()
  return json
}

export const createOrder = async (address, amount) => {
  const response = await fetch(coinswitch_url + 'v2/order', {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': coinswitch_key,
        'x-user-ip': ip_address
      },
      method: 'POST',
      body: JSON.stringify({
        depositCoin: 'btc',
        destinationCoin: 'xlm',
        depositCoinAmount: amount,
        destinationAddress: {
          address,
          tag: null
        }
      })
  })
  const json = await response.json()
  return json
}

export const getOrderStatus = async (orderId) => {
  const response = await fetch(coinswitch_url + 'v2/order/' + orderId, {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': coinswitch_key,
        'x-user-ip': ip_address
      },
      method: 'GET'
  })
  const json = await response.json()
  return json
}