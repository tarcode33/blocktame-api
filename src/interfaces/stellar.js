import Stellar from 'stellar-sdk'
import StellarHDWallet from 'stellar-hd-wallet'
import fetch from 'node-fetch'

require('dotenv').config()

let stellar_url = process.env.STELLAR_SERVER_URL

if (process.env.IS_TESTNET) {
    stellar_url = process.env.STELLAR_TESTNET_SERVER_URL
}

export const server = new Stellar.Server(stellar_url);
Stellar.Network.useTestNetwork()


export const createWallet = () => {
    const mnemonic = StellarHDWallet.generateMnemonic()
    const wallet = StellarHDWallet.fromMnemonic(mnemonic)

    const public_key = wallet.getPublicKey(0)
    const secret = wallet.getSecret(0) 
    

    return {
        mnemonic,
        public_key,
        secret
    }
}

export const fundTestnetAccount = (address) => {
    return fetch('https://horizon-testnet.stellar.org/friendbot?addr=' + address)
    .then(res=>res.json())
}

export const restoreWallet = (mnemonic) => {
    const wallet = StellarHDWallet.fromMnemonic(mnemonic)

    const public_key = wallet.getPublicKey(0)
    const secret = wallet.getSecret(0)

    return {
        public_key,
        secret
    }
}

export const createTransaction = (sourceId, destinationId, amount, memo) => {
    var transaction;

    return server.loadAccount(destinationId)
    .catch(StellarSdk.NotFoundError, function (error) {
        throw new Error('The destination account does not exist!');
    })
    .then(function() {
        return server.loadAccount(sourceId);
    })
    .then(function(sourceAccount) {
        // Start building the transaction.
        transaction = new StellarSdk.TransactionBuilder(sourceAccount)
        .addOperation(StellarSdk.Operation.payment({
            destination: destinationId,
            // Because Stellar allows transaction in many currencies, you must
            // specify the asset type. The special "native" asset represents Lumens.
            asset: StellarSdk.Asset.native(),
            amount
        }))
        // A memo allows you to add your own metadata to a transaction. It's
        // optional and does not affect how Stellar treats the transaction.
        .addMemo(StellarSdk.Memo.text(memo))
        .build();
        // Sign the transaction to prove you are actually the person sending it.
        transaction.sign(sourceKeys);
        // And finally, send it off to Stellar!
        return server.submitTransaction(transaction);
    })
    .then(function(result) {
        console.log('Success! Results:', result);
    })
    .catch(function(error) {
        console.error('Something went wrong!', error);
        // If the result is unknown (no response body, timeout etc.) we simply resubmit
        // already built transaction:
        // server.submitTransaction(transaction);
    });
}
