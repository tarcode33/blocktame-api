import mongoose from 'mongoose'
import crypto from 'crypto'
import jwt from 'jsonwebtoken'

const { Schema } = mongoose;

const UsersSchema = new Schema({
  email: {type: String, lowercase: true },
  first_name: String,
  last_name: String,
  id_number: String,
  merchant: Boolean,
  isVerified: { type: Boolean, default: false },
  hash: String,
  salt: String,
  reset_password_token: String,
  reset_password_expires: String
});

UsersSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UsersSchema.methods.validatePassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UsersSchema.methods.generateJWT = function() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
}

UsersSchema.methods.toAuthJSON = function() {
  return {
    _id: this._id,
    merchant: this.merchant,
    email: this.email,
    first_name: this.first_name,
    last_name: this.last_name,
    id_number: this.id_number,
    token: this.generateJWT(),
  };
};

export const UserModel = mongoose.model('User', UsersSchema);

const TokenSchema = new mongoose.Schema({
  _userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
  token: { type: String, required: true },
  createdAt: { type: Date, required: true, default: Date.now, expires: 43200 }
});

export const TokenModel = mongoose.model('Token', TokenSchema)
