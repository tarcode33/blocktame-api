import mongoose, { mongo } from 'mongoose'
import { UserModel, TokenModel } from './user'

export const User = UserModel

export const Token = TokenModel

const WalletSchema = new mongoose.Schema({
    user_id: String,
    public_key: String,
    type: String,
    email: String,
    created: Date,
    default: Boolean
});

WalletSchema.index({default: 1, user_id: 1 }, {unique: true, partialFilterExpression: {default: true}});

export const Wallet =  mongoose.model(
    'Wallet', 
    WalletSchema
);


const TransactionSchema = new mongoose.Schema({
    amount: String,
    currency: String,
    memo: String,
    to_details: Object,
    from_details: Object,
    to_address: String,
    from_address: String,
    type: String,
    tx_hash: String,
    created: Date
})

WalletSchema.index({default: 1, tx_hash: 1 }, {unique: true, partialFilterExpression: {default: true}});

export const Transaction = mongoose.model(
    'Transaction',
    TransactionSchema
)

const ShopSchema = new mongoose.Schema({
    name: String,
    description: String,
    img_url: String,
    merchantId: String,
    created: Date
})

export const Shop = mongoose.model(
    'Shop',
    ShopSchema
)

const CategorySchema = new mongoose.Schema({
    name: String,
    description: String,
    shopId: String,
    created: Date
})

export const Category = mongoose.model(
    'Category',
    CategorySchema
)

const ProductSchema = new mongoose.Schema({
    name: String,
    description: String,
    categoryId: String,
    price_in_xlm: String,
    qty: Number,
    img_url: String,
    created: Date
})

export const Product = mongoose.model(
    'Product',
    ProductSchema
)